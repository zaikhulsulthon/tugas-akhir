const mqtt = require('mqtt');
const { Pool } = require('pg');
const pool = new Pool({
    host: 'localhost',
    user: 'nama_pengguna',
    password: 'kata_sandi',
    database: 'nama_database',
    port: 5432
});

const mqttBroker = 'mqtt://localhost';
const mqttTopic = 'postgre_data';

const client = mqtt.connect(mqttBroker);

client.on('connect', () => {
    console.log(`Terhubung ke broker MQTT pada ${mqttBroker}`);
});

client.on('error', (err) => {
    console.error(`Error: ${err}`);
});

function sendToMqtt(data) {
    client.publish(mqttTopic, JSON.stringify(data));
}

async function fetchDataFromPostgre() {
    const client = await pool.connect();
    try {
        const result = await client.query('SELECT * FROM todos');
        const rows = result.rows;
        rows.forEach(row => {
            const data = {
                id: row.id,
                nama: row.nama,
                alamat: row.alamat
            };
            sendToMqtt(data);
        });
    } catch (error) {
        console.error(`Error saat mengambil data dari PostgreSQL: ${error}`);
    } finally {
        client.release();
    }
}

setInterval(() => {
    fetchDataFromPostgre();
}, 5000); // ambil data setiap 5 detik

