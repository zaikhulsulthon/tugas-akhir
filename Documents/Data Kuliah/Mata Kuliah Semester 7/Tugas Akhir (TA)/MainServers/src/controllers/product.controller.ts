import { FastifyReply, FastifyRequest } from "fastify";
import { CreateProductInput } from "../schemas/product.schema";
import { createProduct, getProducts} from "../service/product.service";

export async function createProductHandler(
    request: FastifyRequest<{
        Body: CreateProductInput;
    }>
) {
    const product = await createProduct({
        ...request.body,
        ownerId: request.user.id,
    });

    return product;
}

export async function getProductHandler() {
    const products = await getProducts();

    return products;
}