import prisma from "../utils/prisma";
import { CreateProductInput } from "../schemas/product.schema";

export async function createProduct(
    data: CreateProductInput & { ownerId: number }
) {
    return prisma.product.create({
      data,
    });
}

export function getProducts() {
    return prisma.product.findMany({
      select: {
        content: true,
        title: true,
        condition: true,
        id: true,
        createdAt: true,
        updatedAt: true,
        owner: {
          select: {
            name: true,
            id: true,
          },
        },
      },
    });
  }

