import buildServer from "./server";

const server = buildServer();

async function main() {
    try {
        await server.listen(3000, "0.0.0.0");

        console.log(`Server ready at http://localhost:3000`);

        // lakukan koneksi ke broker MQTT setelah server siap
        server.mqtt.on("connect", function() {
            console.log("MQTT broker connected");
        });
    } catch (e) {
        console.error(e);
        process.exit(1);
    }
}

main();



