require('dotenv').config();
require('module-alias/register');

import Fastify, { FastifyRequest, FastifyReply } from "fastify";
import { withRefResolver } from "fastify-zod";
import fjwt, { JWT } from "@fastify/jwt";
import swagger from "@fastify/swagger";
import mqtt from "mqtt"; // import library mqtt

import { userSchemas } from "./src/schemas/user.schema";
import userRoutes from "./src/routes/user.route";
import { productSchemas } from "./src/schemas/product.schema";
import productRoutes from "./src/routes/product.route";
import { version } from "./package.json";

declare module "fastify" {
    interface FastifyRequest {
        jwt: JWT;
    }
    export interface FastifyInstance {
        authenticate: any;
        mqtt: any; // tambahkan deklarasi property mqtt pada FastifyInstance
    }
}

declare module "@fastify/jwt" {
    interface FastifyJWT {
        user: {
            id: number;
            email: string;
            name: string;
        };
    }
}

function buildServer() {
    const server = Fastify();

    server.register(fjwt, {
        secret: "ndkandnan78duy9sau87dbndsa89u7dsy789adb",
    });

    server.decorate(
        "authenticate",
        async (request: FastifyRequest, reply: FastifyReply) => {
            try {
                await request.jwtVerify();
            } catch (e) {
                return reply.send(e);
            }
        }
    );

    server.get("/healthcheck", async function() {
        return { status: "OK" };
    });

    server.addHook("preHandler", (req, reply, next) => {
        req.jwt = server.jwt;
        return next();
    });

    for (const skema of [...userSchemas, ...productSchemas]) {
        server.addSchema(skema);
    }

    server.register(
        swagger,
        withRefResolver({
            routePrefix: "/docs",
            exposeRoute: true,
            staticCSP: true,
            openapi: {
                info: {
                    title: "Server for Algae",
                    description: "API Web Server for Algae",
                    version,
                },
            },
        })
    );

    server.register(userRoutes, { prefix: "api/users" });
    server.register(productRoutes, { prefix: "api/products" });

    // tambahkan implementasi koneksi MQTT
    const mqttClient = mqtt.connect(process.env.MQTT_BROKER_URL);
    server.decorate("mqtt", mqttClient);

    return server;
}

export default buildServer;